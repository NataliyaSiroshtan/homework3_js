//1. Функции - это так называемый "кусок" кода, у которого есть название и этот код можно выполнить, обратившись по названию.
//Если в разных местах кода нужно выполнить одну и ту же команду, то необходимо вынести это в отдельную функцию и вызвать ее, что значительно уменьшит количество кода, который нужно написать.
//2. Аргументы функции передают значения, которые используются в теле функции для выполнения програмнного кода при вызове функции.

let firstUserNumber = +prompt("Введите первое число");
let secondUserNumber = +prompt("Введите второе число");
let operationUser = prompt("Введите действие для двух чисел");
while (!isValidNumber(firstUserNumber)) {
  firstUserNumber = +prompt("Введите первое число");
}
while (!isValidNumber(secondUserNumber)) {
  secondUserNumber = +prompt("Введите второе число");
}

function isValidNumber(value) {
  return !!value;
}

if (operationUser === "+") {
  function getSumm(firstNum, secondNum) {
    let summ = firstNum + secondNum;
    return summ;
  }
  console.log(getSumm(firstUserNumber, secondUserNumber));
} else if (operationUser === "-") {
  function getSubtraction(firstNum, secondNum) {
    let subtraction = firstNum - secondNum;
    return subtraction;
  }
  console.log(getSubtraction(firstUserNumber, secondUserNumber));
} else if (operationUser === "*") {
  function getMultipli(firstNum, secondNum) {
    let multipli = firstNum * secondNum;
    return multipli;
  }
  console.log(getMultipli(firstUserNumber, secondUserNumber));
} else if (operationUser === "/") {
  function getDivision(firstNum, secondNum) {
    let division = firstNum / secondNum;
    return division;
  }
  console.log(getDivision(firstUserNumber, secondUserNumber));
}
